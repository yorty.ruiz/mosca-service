#! /usr/bin/env node

var mqtt    = require('mqtt');
var client  = mqtt.connect('mqtt://93.188.164.227');

client.on('connect', function () {
    client.subscribe('mosca-service.example');
});

client.on('message', function (topic, payload) {
    var message = JSON.parse(payload);
    console.log(message.title + " --> " + message.body);
});