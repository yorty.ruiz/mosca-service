#! /usr/bin/env node

var mqtt    = require('mqtt');
var client  = mqtt.connect('mqtt://93.188.164.227');

client.on('connect', function () {
    var message = JSON.stringify({
        title: 'pepe',
        body: 'Un ejemplo'
    });

    client.publish('mosca-service.example', message, {
        retain: false,
        qa: 1
    });
    client.end();
});
