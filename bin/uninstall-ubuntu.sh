#!/bin/bash


service mosca-service.js stop
update-rc.d -f mosca-service.js remove
if [ -f /etc/init.d/mosca-service.js ]; then
    unlink /etc/init.d/mosca-service.js
fi


