#!/bin/bash

mkdir -p /var/log/mosca-service
if [ -f /etc/init.d/mosca-service.js ]; then
    unlink /etc/init.d/mosca-service.js
fi

if [ ! -f /etc/mosca-service.json ]; then
    ln -s /usr/lib/node_modules/mosca-service/config.json /etc/mosca-service.json
fi

ln -s /usr/bin/mosca-service.js /etc/init.d/mosca-service.js
service mosca-service.js start
update-rc.d mosca-service.js defaults
