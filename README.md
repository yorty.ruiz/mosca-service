# Mosca.JS MQTT Service #

## Install ##

Install for different platforms, by now just Ubuntu

### Ubuntu Installation ###

#### Install ####
````bash
sudo apt-get install nodejs
sudo npm install -g gitlab:yorty.ruiz/mosca-service
sudo mosca-service-install-ubuntu
````

#### Uninstall ####
````bash
sudo mosca-service-uninstall-ubuntu
sudo npm uninstall -g mosca-service
````
