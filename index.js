#! /usr/bin/env node
var mosca = require('mosca');
var startStopDaemon = require('start-stop-daemon');

var config = require('./config.json');

var ascii = "" +
    "       +++.+++:   ,+++    +++;   '+++    +++. \n" +
    "      ++.+++.++   ++.++  ++,'+  `+',++  ++,++ \n" +
    "      +`  +,  +: .+  .+  +;  +; '+  '+  +`  +` \n" +
    "      +`  +.  +: ,+  `+  ++  +; '+  ;+  +   +. \n" +
    "      +`  +.  +: ,+  `+   +'    '+      +   +. \n" +
    "      +`  +.  +: ,+  `+   :+.   '+      +++++. \n" +
    "      +`  +.  +: ,+  `+    ++   '+      +++++. \n" +
    "      +`  +.  +: ,+  `+     ++  '+      +   +. \n" +
    "      +`  +.  +: ,+  `+  +:  +: '+  ;+  +   +. \n" +
    "      +`  +.  +: .+  .+  +;  +; '+  '+  +   +. \n" +
    "      +`  +.  +:  ++;++  ++'++   ++'+'  +   +. \n" +
    "      +`  +.  +:   +++    +++.   ,++'   +   +. ";

console.log(ascii);

startStopDaemon(config.options, function () {
    console.log("Iniciando Mosca.JS MQTT Service...");

    var server = new mosca.Server(config.settings);

    server.on('clientConnected', function (client) {
        console.log('client connected', client.id);
    });

    // fired when a message is received
    server.on('published', function (packet, client) {
        console.log('Published', packet.payload);
    });

    server.on('ready', setup);

    // fired when the mqtt server is ready
    function setup() {
        console.log("Mosca.JS MQTT service is up and running");
    }
});


